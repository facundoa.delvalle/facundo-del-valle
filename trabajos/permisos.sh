#!/bin/bash


#visualización de comandos posibles

ERR1="gestor de permisos, grupos y usuarios\n

\tacciones posibles\n
\tCPerm\tcambiar permisos de archivos o directorios\n
\tCMain\tcambiar dueño de ficheros o directorios\n
\tNgroup\tcrear nuevo grupo\n
\tDgroup\teliminar grupos\n
\tLgroup\tlistar los grupos existentes\n
\tFind\tbuscar ficheros o directorios\n"

#salida cuando quiera cambiar permisos sobre elementos que no le corresponden

ERR2="este elemento no le corresponde, pida los permisos al usuario: $USR"

#comprobar cadenas de argumento
#en el caso que no elija una opcion valida se muestra
#la visualización de comandos posibles

if [ -z "$*" ]; then
    echo -e $ERR1
    exit 1 
fi

#cambiar permisos de archivos o directorios

if [[ $1 == CPerm && $# -ge 3 ]];then
	ARCH="$(echo $* | cut -d ' ' -f 3,4,5,6,7,8,9)"
	chmod -c $2 $ARCH
fi

#cambiar dueño y grupo de ficheros y directorios

if [[ $1 == CMain && $# -ge 4 ]];then
	if [ $UID -ne 0 ]; then
   		echo "Ejecute este programa como 'root'."
	else
		ARCH="$(echo $* | cut -d ' ' -f 4,5,6,7,8,9)"
		chown -c $2:$3 $ARCH
	fi
fi
 
#creacion de grupos

if [[ $1 == NGroup && $# == 2 ]];then
	if [ $UID -ne 0 ]; then
        echo "Ejecute este programa como 'root'."
        else
	groupadd $2
	fi
fi

#eliminacion de grupos

if [[ $1 == DGroup && $# == 2 ]];then
        if [ $UID -ne 0 ]; then
        echo "Ejecute este programa como 'root'."
        else
        groupdel $2
	fi
fi

#ver todos los grupos

if [[ $1 == LGroup ]];then
	GRP="$(cat /etc/group | cut -d ':' -f 1 )"
	echo 'los grupos existentes son:' $GRP	
fi

#buscar ficheros y directorios segun permisos definidos

if [[ $1 == Find && $# == 2 ]];then
	find -perm $2
fi

#exit status

echo 'el exit status es:' $?


